// affichage ouverture/fermeture en couleur green/red

var d = new Date();
var n = d.getDay();
var now = d.getHours() + "." + d.getMinutes();
var weekdays = [
    ["Sunday"],
    ["Monday", 9.00, 12.00, 14.00,19.00],
    ["Tuesday", 9.00, 12.00, 14.00,19.00],
    ["Wednesday", 12.00,13.00, 14.00,19.00],//12.00,13.00
    ["Thursday", 9.00, 12.00, 14.00,19.00],
    ["Friday", 9.00, 11.30],
    ["Saturday", 9.00, 12.00, 14.00,19.00]
];
var day = weekdays[n];


if (now > day[1] && now < day[2] || now > day[3] && now < day[4]) {
    // console.log("Ouvert actuellement");
    document.getElementById('status').innerHTML = "Ouvert \nactuellement";
    document.getElementById('status').style.color ="green";

}
 else {
    // console.log("Fermé actuellement");
    document.getElementById('status').innerHTML = "Fermé \nactuellement";
    document.getElementById('status').style.color ="red";
}

/*===============================*/
/*=== Clic sur le menu burger ===*/
/*===============================*/

// var mainMenu = document.querySelector("#side-panel");
// var burgerMenu = document.querySelector("#menu-burger"); 


// Vérifie si l'événement touchstart existe et est le premier déclenché
// var clickedEvent = "click"; // Au clic si "touchstart" n'est pas détecté
// window.addEventListener('touchstart', function detectTouch() {
// 	clickedEvent = "touchstart"; // Transforme l'événement en "touchstart"
// 	window.removeEventListener('touchstart', detectTouch, false);
// }, false);

// Créé un "toggle class" en Javascrit natif (compatible partout)
// burgerMenu.addEventListener(clickedEvent, function(evt) {
// 	console.log(clickedEvent);
// 	// Modification du menu burger
// 	if(!this.getAttribute("class")) {
// 		this.setAttribute("class", "clicked");
// 	} else {
// 		this.removeAttribute("class");
// 	}
	// Variante avec x.classList (ou DOMTokenList), pas 100% compatible avant IE 11...
	// burgerMenu.classList.toggle("clicked");

	// Créé l'effet pour le menu slide (compatible partout)
// 	if(mainMenu.getAttribute("class") != "visible") {
// 		mainMenu.setAttribute("class", "visible");
// 	} else {
// 		mainMenu.setAttribute("class", "invisible");

//   }
// }
